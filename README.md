ics-ans-role-vagrant
====================

Ansible role to install virtualbox and vagrant.

Requirements
------------

- ansible >= 2.7
- molecule >= 2.19

Role Variables
--------------

```yaml
virtualbox_rpm: VirtualBox-6.1
virtualbox_version: 6.1.12_139181_el7
vagrant_version: 2.2.10
vagrant_user: root
vagrant_install_vbguest: true
vagrant_vm_runtime_limit: 21600 # 6 hours, in seconds
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-vagrant
```

License
-------

BSD 2-clause
---
