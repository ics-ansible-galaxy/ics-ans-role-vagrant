import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('default_group')


def test_default(host):
    assert host.package('VirtualBox-6.1').is_installed
    assert host.package('vagrant').is_installed


def test_vbguest_installed(host):
    cmd = host.run('vagrant plugin list')
    assert 'vagrant-vbguest' in cmd.stdout


def test_virtualbox(host):
    cmd = host.run('VBoxManage --version')
    # If VirtualBox is not installed properly, we get:
    # WARNING: The vboxdrv kernel module is not loaded...
    assert cmd.stdout.startswith('6.1')


def test_script_exists(host):
    assert host.file("/usr/local/bin/clean-virtualbox").exists
